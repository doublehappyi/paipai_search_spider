#!/usr/bin/python
# coding=gbk
import cgi
import os
import config

#####################日志设置#####################
import os, logging, time

log_path = os.path.abspath(os.path.dirname(__file__)) + '/log'
if not os.path.exists(log_path):
    os.mkdir(log_path)
timestamp = time.strftime("%Y-%m-%d", time.localtime())
logging.basicConfig(filename=log_path + '/' + timestamp + '.log', filemode='w', level=logging.DEBUG,
                    format="%(asctime)s: %(levelname)s: %(message)s", encode="gbk", encoding="gbk", )
logger = logging.getLogger(__file__)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
#####################日志设置#####################


#!/usr/bin/python


if __name__ == '__main__':
    base_path = os.path.abspath(os.path.dirname(__file__))
    missions_path = base_path + '/spider/missions'

    form = cgi.FieldStorage()
    file_name = form.getvalue('file_name')
    logger.debug('file_name: %s' % file_name)
    file_path = missions_path+'/'+file_name
    logger.debug('file_path:%s' % file_path)
    # file_name='file1.txt'
    # file_path = base_path+'/spider/file.txt'
    if os.path.exists(file_path):
        # print "Content-type:text/html\r\n\r\n"
        # print'<div>下载成功，文件存在！</div'
        # print "Content-type: text/html;\n\n"
        # print "<html><head><meta http-equiv=\"refresh\" content=\"0; url=%s\" /></head></html>" % file_path

        # print "Content-type: application/octet-stream; filename=\"%s\"\r\n" % file_name;
        # print 'Content-Disposition: attachment; filename=\"%s\"\r\n\n' % file_name;

        print "Content-Disposition: attachment; filename=" + file_name + "\r\n\n";
        print "Content-Type:application/zip; name=" + file_name + "\r\n\n";
        print "Content-Length:%s" % os.path.getsize(file_path)
        print ""

        f = open(file_path, 'rb')
        str = f.read()
        print str
        f.close()
    else:
        print "Content-type:text/html\r\n\r\n"
        print'<div>下载失败，文件不存在！</div>'
        print '<a href="%s">返回</a>' % config.main_page


