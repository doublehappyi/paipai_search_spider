import os

if not os.path.exists(os.path.abspath(os.path.dirname(__file__)) + '/log'):
    os.mkdir(os.path.abspath(os.path.dirname(__file__)) + '/log')