#!/usr/bin/python
# coding=gbk
import cgi
import os
import json
import config

#####################日志设置#####################
import os, logging, time

log_path = os.path.abspath(os.path.dirname(__file__)) + '/log'
if not os.path.exists(log_path):
    os.mkdir(log_path)
timestamp = time.strftime("%Y-%m-%d", time.localtime())
logging.basicConfig(filename=log_path + '/' + timestamp + '.log', filemode='w', level=logging.DEBUG,
                    format="%(asctime)s: %(levelname)s: %(message)s", encode="gbk", encoding="gbk", )
logger = logging.getLogger(__file__)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
#####################日志设置#####################

if __name__ == '__main__':
    print "Content-type:text/json\r\n\r\n"
    base_path = os.path.abspath(os.path.dirname(__file__))
    missions_path = base_path + '/spider/missions'
    result = {}
    result['lists'] = []
    result['success'] = True

    try:
        files = os.popen('ls %s' % missions_path).read().split('\n')
        len_files = len(files)
        if len_files > 0:
            for f in files:
                if f.endswith('.mission'):
                    mission = {}
                    mission['name'] = f.split(".mission")[0]
                    gz_file = f + '.zip'
                    if gz_file in files:
                        mission['status'] = 1
                        mission['url'] = config.base_page+'/download_mission.py?file_name=' + gz_file
                        mission['del_url'] = config.base_page+'/del_mission.py?mission_name=' + mission['name']+'.mission'
                    else:
                        mission['status'] = 0

                    result['lists'].append(mission)

        else:
            result['success'] = True
            result['count'] = 0

    except Exception, e:
        logger.debug('check_mission:Exception:%s' % e)
        result['success'] = False

    json_result = json.dumps(result)
    logger.debug('json_result: %s' % json_result)
    print json_result
