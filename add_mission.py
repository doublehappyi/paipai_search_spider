#!/usr/bin/python
# coding=gbk
import cgi
import os
import config

#####################日志设置#####################
import os, logging, time

log_path = os.path.abspath(os.path.dirname(__file__)) + '/log'
if not os.path.exists(log_path):
    os.mkdir(log_path)
timestamp = time.strftime("%Y-%m-%d", time.localtime())
logging.basicConfig(filename=log_path + '/' + timestamp + '.log', filemode='w', level=logging.DEBUG,
                    format="%(asctime)s: %(levelname)s: %(message)s", encode="gbk", encoding="gbk", )
logger = logging.getLogger(__file__)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
#####################日志设置#####################

if __name__ == '__main__':
    print "Content-type:text/html\r\n\r\n"
    form = cgi.FieldStorage()

    file_item = form['keyword_file']
    if file_item.filename:
        filename = os.path.basename(file_item.filename)
        base_path = os.path.abspath(os.path.dirname(__file__))
        execute_mission_path = base_path + '/execute_mission.sh'
        missions_path = base_path + '/spider/missions'
        curr_mission_path = missions_path + '/' + filename+'.mission'
        file_path = curr_mission_path + '/' + filename

        if not os.path.exists(missions_path):
            try:
                os.mkdir(missions_path, 0777)
            except Exception, e:
                logger.debug('os.mkdir(missions_path) exception %s' % e)
        if os.path.exists(curr_mission_path):
            message = '<html><meta charset="gbk"><div>添加任务失败，任务已经存在！</div><a href="%s">返回</a></html>' % config.main_page
        else:
            os.mkdir(curr_mission_path, 0777)
            f = open(file_path, 'wb')
            f.write(file_item.file.read())
            f.close()
            logger.debug('(execute_mission_path:%s, file_path:%s, base_path:%s)' % (execute_mission_path, file_path, base_path))
            os.system('nohup sh %s %s %s &' % (execute_mission_path, file_path, base_path))
            message = '<html><meta charset="gbk"><div>添加任务成功！</div><a href="%s">返回</a></html>' % config.main_page

    else:
        message = '<html><meta charset="gbk"><div>添加任务失败，没有上传文件!</div><a href="%s">返回</a></html>' % config.main_page

    print message