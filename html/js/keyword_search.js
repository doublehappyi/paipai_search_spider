/**
 * Created by db on 15-8-27.
 */
(function ($) {
    $(function () {
        base_url = '/py-bin/paipai_engine_query/paipai/cgi/keyword_search';
        check_url = base_url + '/check_mission.py';
        clear_mission_url = base_url + '/del_mission.py?mission_name=all';

        init_mission(check_url);

        $('.mission-refresh').click(function(){
            init_mission(check_url);
        });

        $('.mission-clear').click(function () {
            location = clear_mission_url;
        });
    });

    function init_mission(url) {
        $.ajax(url, {
            success: function (data) {
                lists = data['lists']
                if (data.success && lists && lists.length > 0) {
                    html = ''
                    for (var i = 0, len = lists.length; i < len; i++) {
                        curr_list = lists[i];
                        name = curr_list['name'];
                        status = curr_list['status'];
                        status_str = status == 1 ? '<span style="color:green">已完成</span>' : '<span style="color:red">未完成</span>';
                        operation_str = status == 1 ?
                            '<a style="color:green" href="' + curr_list['url']+'">下载</a>' +
                            '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '<a style="color:red" href="'+curr_list['del_url']+'">删除</a>' : '-';
                        html = html + '<tr class="mission-item"> ' +
                            '<td>' + name + '</td> ' +
                            '<td>' + status_str + '</td> ' +
                            '<td>' + operation_str + '</td> ' +
                            '</tr>'
                    }
                    $('.mission-item').remove();
                    $('.mission-table').append(html);
                }
            }
        });
    }

    //function clearMission(url){
    //    $.ajax(url, {
    //        success: function (data) {
    //            if (data.success) {
    //                $('.mission-item').remove();
    //            }
    //        }
    //    });
    //}
})(jQuery);
