#!/usr/bin/python
# coding=gbk
import cgi
import os
import config

#####################日志设置#####################
import os, logging, time

log_path = os.path.abspath(os.path.dirname(__file__)) + '/log'
if not os.path.exists(log_path):
    os.mkdir(log_path)
timestamp = time.strftime("%Y-%m-%d", time.localtime())
logging.basicConfig(filename=log_path + '/' + timestamp + '.log', filemode='w', level=logging.DEBUG,
                    format="%(asctime)s: %(levelname)s: %(message)s", encode="gbk", encoding="gbk", )
logger = logging.getLogger(__file__)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
#####################日志设置#####################


#!/usr/bin/python

if __name__ == '__main__':
    base_path = os.path.abspath(os.path.dirname(__file__))
    missions_path = base_path + '/spider/missions'

    form = cgi.FieldStorage()
    mission_name = form.getvalue('mission_name')
    mission_path = missions_path+'/'+mission_name
    mission_zip_path = mission_path+'.zip'
    if os.path.exists(mission_path):
        os.system('rm -rf %s' % mission_path)
        if os.path.exists(mission_zip_path):
            os.system('rm -f %s' % mission_zip_path)

        print "Content-type:text/html\r\n\r\n"
        print '<html><meta charset="gbk"><div>删除任务成功！</div><a href="%s">返回</a></html>' % config.main_page
    elif mission_name == 'all':
        #结束所有main.py进程
        os.system('sh %s' % base_path+'/kill_main.sh')
        time.sleep(2)
        os.system('rm -rf %s' % missions_path + '/*')
        print "Content-type:text/html\r\n\r\n"
        print '<html><meta charset="gbk"><div>清空任务成功！</div><a href="%s">返回</a></html>' % config.main_page
    else:
        print "Content-type:text/html\r\n\r\n"
        print '<html><meta charset="gbk"><div>删除任务失败，任务不存在！</div><a href="%s">返回</a></html>' % config.main_page



