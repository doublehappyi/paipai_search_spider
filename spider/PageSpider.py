# coding:gbk
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('gbk')
import threading
import urllib2
import json
import re


#####################日志设置#####################
import utils
import os
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
#####################日志设置#####################


class PageSpiderManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(PageSpiderWorker(self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('PageSpiderManager:Putting (None, None) into p_queue')
            self.p_queue.put((None, None, None))


class PageSpiderWorker(threading.Thread):
    def __init__(self, c_queue, p_queue):
        super(PageSpiderWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.setDaemon(True)
        self.start()

    def run(self):
        proxy = urllib2.ProxyHandler({'http': '10.137.152.17'})
        opener = urllib2.build_opener(proxy)
        urllib2.install_opener(opener)

        while 1:
            try:
                logger.debug("PageSpiderWorker:get from cqueue")
                retry, line, url = self.c_queue.get()
                logger.debug("PageSpiderWorker:get from ok")
                if url:
                    i = 0
                    while 1:
                        if i > 50:
                            logger.debug('PageSpiderWorker: urllib2 failed, url:%s' % url)
                            break
                        try:
                            response = urllib2.urlopen(url).read()
                            if response:
                                logger.debug('PageSpiderWorker: start handle_response')
                                self.handle_response(response, line, url)
                                break
                        except urllib2.HTTPError, e:
                            i+=1
                            logger.warning('PageSpiderWorker: Retry times, url, urllib2.HTTPError:%s, %s, %s' % (i, url, e))
                        except Exception, e:
                            logger.debug('PageSpiderWorker: urllib2: Exception %s Url:%s' % (e, url))
                else:
                    logger.debug('PageSpiderWorker: no more data in c_queue')
                    break
            except Exception, e:
                logger.debug('PageSpiderWorker: Exception %s Url:%s' % (e, url))
            finally:
                self.c_queue.task_done()

    def handle_response(self, response, line, url):
        try:
            json_str = ''.join(response.replace('\n', '').replace('\r', '').strip().split()).replace('try{jsonp1(','').replace(');}catch(e){}', '')
            try:
                json_str=json_str.decode('gbk')
                data = json.loads(json_str)
                data_list = data['data']['list']
                if len(data_list):
                    if len(data_list) < 60:
                        logger.debug("less than 60====%s" % len(data_list))
                    for item in data_list:
                        commId = item['commId']
                        title = item['title'].replace('<em>','').replace('</em>','')
                        logger.debug('PageSpiderWorker:handle_response: Putting into p_queue(%s, %s, %s)' % (line, commId, title))
                        self.p_queue.put((line, commId, title))
                else:
                    logger.debug('EmptyUrl:url:'+ url+'json_str:'+json_str)
                    # logger.debug('EmptyUrl:url:'+ url)

            except Exception, e:
                logger.error('PageSpiderWorker: Exception handle_response json.loads error')
                # logger.debug('ErrorEncode:url:'+url+'json_str:'+json_str)

                try:
                    p_title = re.compile('(?<=title\":\").*?(?=\",\"titleH)')
                    p_commId = re.compile('(?<=commId\":\").*?(?=\",\"LeafClassId)')
                    titles = p_title.findall(json_str)
                    commIds = p_commId.findall(json_str)

                    for i in xrange(len(titles)):
                        commId = commIds[i]
                        title = titles[i].replace('<em>','').replace('</em>','')
                        logger.debug('PageSpiderWorker:handle_response: Putting into p_queue (%s, %s, %s)' % (line, commId, title))
                        self.p_queue.put((line, commId, title))
                        logger.debug('EncodeOK:%s\t%s' % (commId, title))
                except Exception, e:
                    logger.debug("PageSpiderWorker:Exception in regex:%s" % e)

        except Exception, e:
            # logger.debug('PageSpiderWorker:handle_response: Exception:%s\nline:\njson_str:\n' % (e, line, json_str))
            logger.debug('PageSpiderWorker:handle_response: Exception:%s\nline:%s' % (e, line))

