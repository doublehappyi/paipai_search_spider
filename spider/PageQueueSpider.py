# coding:gbk
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('gbk')
import threading
import urllib2
import json

#####################日志设置#####################
import utils
import os
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
#####################日志设置#####################


class PageQueueSpiderManager(object):
    def __init__(self, c_queue, p_queue, p_queue2, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_queue2 = p_queue2
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(PageQueueSpiderWorker(self.c_queue, self.p_queue, self.p_queue2))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('PageManager:Putting (None, None) into p_queue')
            self.p_queue.put((None, None, None))


class PageQueueSpiderWorker(threading.Thread):
    def __init__(self, c_queue, p_queue, p_queue2):
        super(PageQueueSpiderWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_queue2 = p_queue2
        self.setDaemon(True)
        self.start()

    def run(self):
        proxy = urllib2.ProxyHandler({'http': '10.137.152.17'})
        opener = urllib2.build_opener(proxy)
        urllib2.install_opener(opener)
        while 1:
            try:
                retry, line, raw_url = self.c_queue.get()
                logger.debug('PageQueueSpiderWorker:c_queue.get (%s, %s)' % (line, raw_url))
                if not raw_url:
                    logger.debug('PageQueueSpiderWorker: no more data in c_queue')
                    break
                url = raw_url + '&PageSize=60&PageNum=1'
                i = 0
                while 1:
                    if i > 50:
                        logger.debug('PageQueueSpiderWorker: urllib2 failed, url:%s' % url)
                        break
                    try:
                        response = urllib2.urlopen(url).read()
                        if response:
                            logger.debug('PageQueueSpiderWorker: start handle_response %s' % line)
                            self.handle_response(response, line, raw_url)
                            break
                    except urllib2.HTTPError, e:
                        i+=1
                        logger.warning('PageQueueSpiderWorker: Retry times, url, urllib2.HTTPError:%s, %s, %s' % (i, url, e))
                    except Exception, e:
                        logger.debug('PageQueueSpiderWorker: urllib2: Exception %s Url:%s' % (e, url))
            except Exception, e:
                logger.debug('PageQueueSpiderWorker: Exception %s Url:%s'(e, url))
            finally:
                self.c_queue.task_done()


    def handle_response(self, response, line, raw_url):
        PageSize = 60
        try:
            json_str = ''.join(response.replace('\n', '').replace('\r', '').strip().split()).replace('try{jsonp1(','').replace(');}catch(e){}', '')
            data = json.loads(json_str, 'gbk')
            totalNum = int(data['data']['totalNum'])
            totalPageNum = int((totalNum + PageSize - 1) / PageSize)

            logger.debug('line, totalNum, PageSize, totalPageNum:%s, %s, %s, %s' % (line, totalNum, PageSize, totalPageNum))
            if totalPageNum > 0:
                for i in xrange(1, totalPageNum + 1):
                    url = raw_url + '&PageSize=' + str(PageSize) + '&PageNum=' + str(i)
                    logger.debug('PageQueueSpiderWorker:handle_response:Putting into p_queue(0, %s, %s)' % (line, url))
                    self.p_queue.put((0, line, url))
            else:
                url = raw_url + '&PageSize=' + str(PageSize) + '&PageNum=1'
                logger.debug('PageQueueSpiderWorker:handle_response:Putting into p_queue zeronum(0, %s, %s)' % (line, url))
                self.p_queue2.put((line, "URL", url))

        except Exception, e:
            logger.debug('PageQueueSpiderWorker:handle_response: Exception:%s' % e)

