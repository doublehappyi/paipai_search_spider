# coding:gbk
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('gbk')
import threading
import codecs

#####################日志设置#####################
import utils
import os
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
#####################日志设置#####################


class ResultWriterWorker(threading.Thread):
    def __init__(self, c_queue, result_dir_path):
        super(ResultWriterWorker, self).__init__()
        self.c_queue = c_queue
        self.result_dir_path = result_dir_path
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug('ResultWriterWorker: c_queue get start')
                line, commId, title = self.c_queue.get()
                logger.debug('ResultWriterWorker: c_queue get over %s, %s, %s' % (line, commId, title ))
                if commId is not None:
                    logger.debug('ResultWriterWorker:commId is %s' % commId)
                    line = line.strip()
                    path = self.result_dir_path + '/' + line.encode('gbk') + '.txt'
                    f = codecs.open(path, 'a', 'gbk')
                    f.write(commId+'\t'+ title +'\r\n')
                    f.flush()
                    f.close()
                else:
                    logger.debug('ResultWriterWorker: no more data in c_queue %s, %s, %s' % (line, commId, title))
                    break
            except Exception, e:
                logger.debug('ResultWriterWorker: Exception %s, line:%s commId:%s title:%s' % (e, line, commId, title))
            finally:
                self.c_queue.task_done()



