# coding:gbk
__author__ = 'db'
import sys
reload(sys)
sys.setdefaultencoding('gbk')
import threading
import urllib2, urllib
import codecs
import Queue
import json

#####################日志设置#####################
import os, logging, time

if not os.path.exists(os.getcwd() + '/log'):
    os.mkdir(os.getcwd() + '/log')
timestamp = time.strftime("%Y-%m-%d", time.localtime())
logging.basicConfig(filename=os.getcwd() + '/log/' + timestamp + '.log', filemode='w', level=logging.DEBUG,
                    format="%(asctime)s: %(levelname)s: %(message)s",encode="gbk" , encoding = "gbk",)
logger = logging.getLogger(__file__)
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)
#####################日志设置#####################


# url = 'http://sse1.paipai.com/comm_json?ClassDirect=1&PageSize=10&PageNum=1&charset=gbk&dtype=jsonp&g_tk=12&KeyWord=%25CA%25D6%25BB%25FA&OrderStyle=77&ac=1&sf=2007&cluster=1&_=1440062888730&callback=jsonp1'

class SearchManager(object):
    def __init__(self, c_queue, c_thread_num=1):
        self.c_queue = c_queue
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            logger.debug('Starting Thread %s ' % i)
            self.threads.append(SearchWorker(self.c_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()


class SearchWorker(threading.Thread):
    def __init__(self, c_queue):
        super(SearchWorker, self).__init__()
        self.c_queue = c_queue
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug("Getting data from c_queue ...")
                retry, keyword, url = self.c_queue.get()
                self.c_queue.task_done()
                logger.debug("Getting data from c_queue OK...")

                if url:
                    self.handle_request(keyword, url)
                else:
                    logger.debug("SearchWorker: no more data in c_queue")
                    break
            except urllib2.HTTPError, e:
                logger.error("urllib2.HTTPError in SearchWorker: %s" % e)
                if retry < 2:
                    retry += 1
                    logger.error("Retry the %s times %s in SearchWorker" % (retry, url))
                    self.c_queue.put((retry, url))
                else:
                    logger.error("SearchWorker: Failed to get %s" % url)
            except Exception, e:
                logger.error("Exception in SearchWorker: %s: url:%s" % (e, url))
                if retry < 2:
                    retry += 1
                    logger.error("Retry the %s times %s in SearchWorker" % (retry, url))
                    self.c_queue.put((retry, url))
                else:
                    logger.error("SearchWorker: Failed to get %s" % url)
    def handle_request(self, keyword, url):
        PageNum = 0
        PageSize = 10
        if not os.path.exists(os.getcwd() + '/data'):
            os.mkdir(os.getcwd() + '/data')
        f = open(os.getcwd() + '/data/'+keyword+'.txt', 'w')
        retry = 0
        finish_flag = 0
        proxy = urllib2.ProxyHandler({'http': '10.137.152.17'})
        opener = urllib2.build_opener(proxy)
        urllib2.install_opener(opener)

        data_str = ''
        while 1:
            PageNum+=1
            try:
                page_url = url + "&PageNum="+str(PageNum) + '&PageSize='+str(PageSize)

                logger.debug("Getting Page %s" % page_url)
                data = urllib2.urlopen(page_url).read().strip()
                logger.debug("Getting Page %s OK" % page_url)
                # logger.debug("HTML%s" % data)
                data = ''.join(data.replace('\n', '').replace('\r','').strip().split()).replace('try{jsonp1(', '').replace(');}catch(e){}','')

                try:
                    data = json.loads(data.decode('gbk').encode('utf-8'), 'utf-8')
                except Exception, e:
                    # logger.error('Exception in json.loads: \ne:%s, \n data:%s' % (e, str(data)))
                    logger.error('Exception in json.loads:\n url: %s, keyword:%s' % (url, keyword))
                    continue


                data_list = data['data']['list']
                logger.debug('keyword, pageNum, list_length, totalNum:,%s, %s, %s, %s' % (keyword, PageNum, len(data_list), data['data']['totalNum']))

                for item in data_list:
                    commId = item['commId']
                    title = item['title'].replace('<em>','').replace('</em>','')

                    try:
                        data_str = commId+'\t'+ title.decode('gbk','ignore') +'\n'
                        logger.debug('Writing into file,content: %s, %s'% (keyword+'.txt', data_str))
                        f.write(data_str)
                        logger.debug('Writing into file,content: %s, %s'% (keyword+'.txt', data_str))
                    except Exception, e:
                        logger.error('Exception in data_str: \ne:%s, \n url:%s,\n%s, \n%s' % (e, url, keyword, data_str))

                if len(data_list) == 0:
                    finish_flag +=1
                    logger.debug('list empty')
                else:
                    finish_flag = 0

                if finish_flag > 5:
                    logger.debug('no more data in keyword %s, currPage is %s,' % (keyword, PageNum))
                    break
                retry = 0
            except urllib2.HTTPError, e:
                #每一个失败请求，多请求一次
                if retry < 2:
                    logger.warning('Retry times %s: page_url:%s' % (retry, page_url))
                    retry += 1
                    PageNum -= 1
            except KeyError, e:
                logger.debug("KeyError e: %s, url:%s, keyword:%s\n data_str:%s" % (e, url, keyword, data_str))
            except Exception, e:
                logger.debug("handle_request: Exception %s, url:%s, keyword:%s\n data_str:%s" % (e, url, keyword, data_str))

        f.flush()
        f.close()


def generate_url_from_keyword(line):
    lines = line.split('-')
    lines_len = len(lines)

    KeyWord = lines[0].strip()
    logger.debug('KeyWord=%s' % KeyWord)
    if KeyWord:
        url = 'http://sse1.paipai.com/comm_json?ClassDirect=1&charset=gbk&dtype=jsonp&g_tk=12&KeyWord=' + urllib.quote(KeyWord.encode('gbk', 'ignore')) + '&OrderStyle=77&ac=1&sf=2007&cluster=1&_=1440062888730&callback=jsonp1'
    else:
        return None
    if lines_len > 1:
        MultiCat= lines[1].strip()
        logger.debug("MultiCat=%s" % MultiCat)
        url = url + '&MultiCat='+ MultiCat

    return url


def generate_start_queue(file_path):
    q = Queue.Queue()
    f = codecs.open(file_path, 'r', 'gbk')
    while 1:

        line = f.readline().strip()
        if line:
            url = generate_url_from_keyword(line)
            logger.debug('Putting into queue (0, %s, %s) ' % (line, url))
            if url:
                q.put((0, line, url))
        else:
            break
    return q

if __name__ == '__main__':
    file_path = os.getcwd()+'/data.txt'
    q = generate_start_queue(file_path)

    #最多跑20个线程
    max_thread_num = 50
    c_thread_num = q.qsize()
    if c_thread_num > max_thread_num:
        c_thread_num = max_thread_num
    for i in range(c_thread_num):
        logger.debug('Putting the %s (None,None,None into q)' % i)
        q.put((None, None, None))

    #启动线程
    search_mgr = SearchManager(c_queue=q, c_thread_num=c_thread_num)
    search_mgr.join()

    logger.debug('Finished!')