# coding:gbk
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('gbk')
import urllib
import codecs
import Queue

#####################日志设置#####################
import utils
import os
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)
#####################日志设置#####################

from PageQueueSpider import PageQueueSpiderManager
from PageSpider import PageSpiderManager
from ResultWriter import ResultWriterWorker


def generate_line_queue(file_path, p_thread_num):
    q = Queue.Queue()
    f = codecs.open(file_path, 'r', 'gbk')
    base_url = 'http://sse1.paipai.com/comm_json?ReverseProperty=0&ClassDirect=1&charset=gbk&dtype=jsonp&g_tk=12&OrderStyle=100&ac=1&sf=2007&cluster=1&_=1440062888730&callback=jsonp1'
    while 1:
        line = f.readline()
        if line:
            line = line.replace('\r','').replace('\n', '').strip()
            logger.debug('generate_line_queue LINE:%s' % line)
            if not line:
                continue
            fields = line.split('-')
            fields_len = len(fields)
            KeyWord = fields[0].strip()
            logger.debug('KeyWord=%s' % KeyWord)
            if KeyWord:
                raw_url = base_url + '&KeyWord=' + urllib.quote(KeyWord.encode('gbk', 'ignore'))

            if fields_len > 1:
                MultiCat = fields[1].strip()
                logger.debug("MultiCat=%s" % MultiCat)
                raw_url = raw_url + '&MultiCat=' + MultiCat

            q.put((0, line, raw_url))
        else:
            break

    for i in range(p_thread_num):
        q.put((None, None, None))
    return q


if __name__ == '__main__':
    #
    if len(sys.argv) > 1:
        file_path = sys.argv[1]
    else:
        file_path = os.path.abspath(os.path.dirname(__file__)) + '/data/data.txt'

    result_dir_path = os.path.dirname(file_path)

    pg_queue_thread_num = 1
    page_thread_num = 50
    result_thread_num = 1

    line_queue = generate_line_queue(file_path, pg_queue_thread_num)
    page_queue = Queue.Queue()
    result_queue = Queue.Queue()

    pg_queue_mgr = PageQueueSpiderManager(c_queue=line_queue, p_queue=page_queue, p_queue2=result_queue, c_thread_num=pg_queue_thread_num,
                                          p_thread_num=page_thread_num)

    pg_mgr = PageSpiderManager(c_queue=page_queue, p_queue=result_queue, c_thread_num=page_thread_num,
                               p_thread_num=result_thread_num)

    result_worker = ResultWriterWorker(c_queue=result_queue, result_dir_path=result_dir_path)

    pg_queue_mgr.join()
    pg_mgr.join()
    result_worker.join()
    logger.debug('stat data')
    # os.system('python '+os.path.abspath(os.path.dirname(__file__))+'/stat.py')
    logger.debug('Finished !')
