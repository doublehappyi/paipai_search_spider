#coding:gbk
import sys
reload(sys)
sys.setdefaultencoding('gbk')
import codecs
import os
import datetime

f_data = codecs.open(os.path.abspath(os.path.dirname(__file__))+'/data2.txt', 'r')
f_stat = codecs.open(os.path.abspath(os.path.dirname(__file__))+'/stat.txt', 'w')
f_stat.write('%-10s\t\t%-10s\t\t%-10s\t\t%-10s\n' % ('Line', 'RealNum', 'ExpectNum', 'GapNum'))
def get_file_line_num(file_path):
    count = 0
    f = open(file_path,'r')
    while 1:
        line = f.readline()
        if line:
            if line.strip():
                count+=1
        else:
            break
    return count

f_log = os.path.abspath(os.path.dirname(__file__)) + '/log/' + datetime.datetime.now().strftime('%Y-%m-%d') + '.log'
expect_str = os.popen("grep 'line, totalNum, PageSize, totalPageNum:' %s| awk -F ':' '{print $6}' | awk -F ',' '{print $1, $2}'" % f_log).read()
expect_str_list = expect_str.decode('gbk').split('\n')
d = {}
for s in expect_str_list:
    if s:
        s_pair = s.split()
        key = s_pair[0].strip()
        num = s_pair[1].strip()
        d[key] = num

while 1:
    line = f_data.readline()

    if line:
        line = line.replace('\n','').strip()
        if line:
            line_file = os.path.abspath(os.path.dirname(__file__))+'/data/'+line.decode('gbk')+'.txt'
            if os.path.exists(line_file):
                real_line_num = get_file_line_num(line_file)
                expect_line_num = 0
                f_stat.write('%-10s\t\t%-10s\t\t%-10s\t\t%-10s\n' %( line, real_line_num, d[line.decode('gbk')], int(d[line.decode('gbk')])-real_line_num))
    else:
        break

