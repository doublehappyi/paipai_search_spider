#coding:gbk
__author__ = 'yishuangxi'
def get_logger(log_path):
    import os, logging, time
    log_file_name = time.strftime("%Y%m%d-%H-%M", time.localtime())
    log_format = "%(asctime)s: %(levelname)s: %(message)s"
    if not os.path.exists(log_path):
        os.mkdir(log_path)

    logging.basicConfig(filename=log_path + '/' + log_file_name + '.log', filemode='w', level=logging.DEBUG,
                        format=log_format)
    logger = logging.getLogger(__file__)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger