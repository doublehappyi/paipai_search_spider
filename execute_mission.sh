#!/bin/sh
base_path=$2
spider_path=${base_path}/spider
spider_main_path=${spider_path}/main.py
missions_path=${spider_path}/missions

file_path=$1
#上传文件名
file_name=`basename $file_path`
#当前任务目录路径
curr_mission_dir_path=`dirname $file_path`
#当前任务目录名
curr_mission_dir_name=`basename $curr_mission_dir_path`

#执行抓取任务
python ${spider_main_path} $file_path

#任务抓取完成，执行文件打包任务
#进入任务目录
cd ${missions_path}
#打包任务目录
zip -r ./${curr_mission_dir_name}.zip ./${curr_mission_dir_name}